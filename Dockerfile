FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ENV VERSION=3.0.1 \
    APP_ENV=production

WORKDIR /app/code

# when external repo is added, apt-get will install the latest in case of conflicting name. apt-cache policy <name> will show what is getting used
# so the remove of 7.4 is probably superfluous but here for completeness
RUN apt-get remove -y php-* php7.4-* libapache2-mod-php7.4 && \
    apt-get autoremove -y && \
    add-apt-repository --yes ppa:ondrej/php && \
    apt update && \
    apt install -y php8.1 php8.1-{apcu,bcmath,bz2,cgi,cli,common,curl,dba,dev,enchant,fpm,gd,gmp,gnupg,imagick,imap,interbase,intl,ldap,mailparse,mbstring,mysql,odbc,opcache,pgsql,phpdbg,pspell,readline,redis,snmp,soap,sqlite3,sybase,tidy,uuid,xml,xmlrpc,xsl,zip,zmq} libapache2-mod-php8.1 && \
    apt install -y php-{date,pear,twig,validate} && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# https://getcomposer.org/download/
RUN curl --fail -L https://getcomposer.org/download/2.6.5/composer.phar -o /usr/bin/composer && chmod +x /usr/bin/composer

# this binaries are not updated with PHP_VERSION since it's a lot of work
RUN update-alternatives --set php /usr/bin/php8.1 && \
    update-alternatives --set phar /usr/bin/phar8.1 && \
    update-alternatives --set phar.phar /usr/bin/phar.phar8.1 && \
    update-alternatives --set phpize /usr/bin/phpize8.1 && \
    update-alternatives --set php-config /usr/bin/php-config8.1

RUN git clone https://github.com/THM-Health/PILOS.git . && \
    git checkout v$VERSION && \
    composer install && \
    npm install && \
    npm run build

RUN mv storage storage_default && \
    ln -s /app/data/storage storage && \
    ln -s /app/data/.env .env

# the /app/data/resources folder seems to be needed in /app/code and can not be moved somwhere else
# put /app/data/resources into runtimeDirs for now
#
# Error from app
#
# [vite]: Rollup failed to resolve import "axios" from "/app/data/resources/js/api/base.js".
# This is most likely unintended because it can break your application at runtime.
# If you do want to externalize this module explicitly add it to
# `build.rollupOptions.external`
# RUN mv resources resources_default && \
#     ln -s /app/data/resources resources

RUN rm -rf /var/www/html && ln -s /app/code/ /var/www/html && \
    rm /etc/nginx/sites-enabled/default && \
    ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default && \
    ln -s /usr/sbin/php-fpm8.1 /usr/local/sbin/php-fpm

COPY Docker /

RUN chmod +x /usr/local/etc/scheduler/run.sh && \
    chmod +x /app/code/frontend/run.sh && \
    ln -s /app/code/frontend /usr/local/etc/frontend && \
    chown www-data:www-data /app/code/frontend/ && \
    ln -s /app/data/ldap_mapping.json /app/code/app/Auth/config/ldap_mapping.json && \
    rm -rf /var/log && ln -s /run/log /var/log && \
    ln -s /run/nginx/scgi /var/lib/nginx/scgi && \
    ln -s /run/nginx/uwsgi /var/lib/nginx/uwsgi && \
    ln -s /run/nginx/body /var/lib/nginx/body && \
    ln -s /run/nginx/fastcgi /var/lib/nginx/fastcgi && \
    ln -s /run/nginx/proxy /var/lib/nginx/proxy && \
    ln -s /app/code/storage/app/public/ /app/code/public/storage

CMD [ "/app/code/start.sh" ]