#!/bin/bash

set -x

# Ensure /app/data exists
mkdir -p /app/data
cd /app/data/ || echo "Something failed!"

# copy default .env file to /app/data if not existent
if [[ ! -f /app/data/.env ]]; then
    cp /app/code/.env.example /app/data/.env
    FIRSTRUN=true
else
    FIRSTRUN=false
fi

if [[ ! -d storage ]]; then
    cp -r /app/code/storage_default storage
fi

if [[ ! -d theme ]]; then
    echo '=> theme folder missing creating and adding defaults'
    mkdir theme
    rsync -arlP /var/www/html/resources/sass/theme/default/ theme/
    rsync -arlP theme/ /var/www/html/resources/sass/theme/custom/
else
    echo '=> Folder exists. Syncing theme to custom'
    rsync -arlP theme/ /var/www/html/resources/sass/theme/custom/
fi

cd /app/code/

# First run
if [[ ${FIRSTRUN} == "true" ]]; then
    echo "=> First run setting you up. . ."
    cd /app/code/ || echo "Something went wrong!"
    # create app key
    php artisan key:generate --no-interaction --force

    echo "=> Create default ldap_mapping.json"
    echo '{"attributes":{"external_id":"cn","first_name":"givenname","last_name":"sn","email":"mail","groups":"memberof"},"roles":[{"name":"user","disabled":false,"rules":[{"attribute":"external_id","regex":"/^.*/i"}]}]}' > /app/data/ldap_mapping.json

    echo "=> Set Database Config"
    sed -e "s/DB_CONNECTION=.*/DB_CONNECTION=pgsql/g" \
        -e "s/DB_HOST=.*/DB_HOST=$CLOUDRON_POSTGRESQL_HOST/g" \
        -e "s/DB_PORT=.*/DB_PORT=$CLOUDRON_POSTGRESQL_PORT/g" \
        -e "s/DB_DATABASE=.*/DB_DATABASE=$CLOUDRON_POSTGRESQL_DATABASE/g" \
        -e "s/DB_USERNAME=.*/DB_USERNAME=$CLOUDRON_POSTGRESQL_USERNAME/g" \
        -e "s/DB_PASSWORD=.*/DB_PASSWORD=$CLOUDRON_POSTGRESQL_PASSWORD/g" \
        -i /app/data/.env

    echo "=> Set Mail Config"
    sed -e "s/MAIL_MAILER=.*/MAIL_MAILER=smtp /g" \
        -e "s/MAIL_HOST=.*/MAIL_HOST=$CLOUDRON_MAIL_SMTP_SERVER/g" \
        -e "s/MAIL_PORT=.*/MAIL_PORT=$CLOUDRON_MAIL_SMTP_PORT/g" \
        -e "s/MAIL_USERNAME=.*/MAIL_USERNAME=$CLOUDRON_MAIL_SMTP_USERNAME/g" \
        -e "s/MAIL_PASSWORD=.*/MAIL_PASSWORD=$CLOUDRON_MAIL_SMTP_PASSWORD/g" \
        -e "s/MAIL_ENCRYPTION=.*/MAIL_ENCRYPTION=null/g" \
        -e "s/MAIL_FROM_ADDRESS=.*/MAIL_FROM_ADDRESS=$CLOUDRON_MAIL_FROM/g" \
        -e "s/MAIL_FROM_NAME=.*/MAIL_FROM_NAME=$CLOUDRON_MAIL_FROM_DISPLAY_NAME/g" \
        -i /app/data/.env
    
    echo "=> Set LDAP Config"
    sed -e "s/LDAP_ENABLED=.*/LDAP_ENABLED=true/g" \
        -e "s/LDAP_HOST=.*/LDAP_HOST=$CLOUDRON_LDAP_HOST/g" \
        -e "s/LDAP_USERNAME=.*/LDAP_USERNAME=$CLOUDRON_LDAP_BIND_DN/g" \
        -e "s/LDAP_PASSWORD=.*/LDAP_PASSWORD=$CLOUDRON_LDAP_BIND_PASSWORD/g" \
        -e "s/LDAP_PORT=.*/LDAP_PORT=$CLOUDRON_LDAP_PORT/g" \
        -e "s/LDAP_BASE_DN=.*/LDAP_BASE_DN=$CLOUDRON_LDAP_USERS_BASE_DN/g" \
        -e "s/#LDAP_GUID_KEY=.*/LDAP_GUID_KEY=username/g" \
        -e "s/\#LDAP_OBJECT_CLASSES=.*/LDAP_OBJECT_CLASSES=top\,person\,organizationalperson\,inetorgperson/g" \
        -e "s/\#LDAP_LOGIN_ATTRIBUTE=.*/LDAP_LOGIN_ATTRIBUTE=username/g" \
        -i /app/data/.env
    
    echo "=> Set Proxy"
    sed -e "s/TRUSTED_PROXIES=.*/TRUSTED_PROXIES=*/g" \
        -i /app/data/.env
    
    echo "=> Set APP_RUL"
    sed -e "s/APP_URL=.*/APP_URL=http:\/\/$CLOUDRON_APP_DOMAIN/g" -i /app/data/.env
    # init database
    php artisan migrate --no-interaction --force
    php artisan db:seed --no-interaction --force

    # Prompt to create inital admin
    echo "=> To create your inital admin user run: php artisan users:create:admin"
fi

function changePermissions() {
    echo "Changing permissions"
    chown -R www-data:www-data /var/www/html/storage/
    chown -R :www-data /var/www/html/resources/custom
    chown -R :www-data /var/www/html/resources/sass/theme/custom
    find /var/www/html/storage/ -type f -exec chmod 664 {} \;
    find /var/www/html/storage/ -type d -exec chmod 775 {} \;
    find /var/www/html/resources/custom/ -type f -exec chmod 664 {} \;
    find /var/www/html/resources/custom/ -type d -exec chmod 775 {} \;
    find /var/www/html/resources/sass/theme/custom/ -type f -exec chmod 664 {} \;
    find /var/www/html/resources/sass/theme/custom/ -type d -exec chmod 775 {} \;
}

function linkStorage() {
    php artisan storage:link
}

function optimize() {
    composer dump-autoload -o
    php artisan config:cache
    php artisan route:cache
    php artisan view:cache
    php artisan locales:cache
}

function buildFrontend() {
    echo "Building frontend"
	/usr/local/etc/frontend/run.sh
}

function migrateDatabase() {
    echo "Migrating database and seed with example data"

    while ! php artisan db:check; do
        echo "Waiting before trying again..."
        sleep 5
    done
    php artisan migrate --force --seed
}

changePermissions
buildFrontend
migrateDatabase
linkStorage
optimize

echo "=> Ensure runtime dirs exist"
mkdir -p /run/php/ \
    /var/log/nginx/ \
    /run/nginx/scgi \
    /run/nginx/uwsgi \
    /run/nginx/body \
    /run/nginx/fastcgi \
    /run/nginx/proxy \
    /var/log/supervisor

exec /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
